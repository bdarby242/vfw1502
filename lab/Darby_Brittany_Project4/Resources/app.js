//WINDOWS
 
var mainWin=Ti.UI.createWindow({  //where all three buttons will show up.
 backgroundImage:"vs2.jpg",
 });
 
 
var imageWin=Ti.UI.createWindow({  //this is where the random images of the models will appear.
 backgroundImage:"vs4.jpg",
 });

var dataWin=Ti.UI.createWindow({  //this is where the information about the models will go.
	backgroundImage:"vs5.jpg",
});

var vidWin=Ti.UI.createWindow({ //this will a video of the VS fashion show.
	backgroundImage:"vs3.jpg",
});


//NAVIGATION WINDOW
var pages=Ti.UI.iOS.createNavigationWindow({   //this is the navigation window
	window:mainWin, 
	
});


//BUTTONS
var models=Ti.UI.createButton({
	title:"Models",
	color:"#287177",
	font:{fontSize:25,fontFamily:"Baskerville-BoldItalic"},
	bottom:110,
});

var info=Ti.UI.createButton({
	title:"Information",
	color:"##287177",
	font:{fontSize:25,fontFamily:"Baskerville-BoldItalic"},
	bottom:60,
});

var video=Ti.UI.createButton({
	title:"Extras",
	color:"#287177",
	font:{fontSize:25,fontFamily:"Baskerville-BoldItalic"},
	bottom:10,
});

//EVENT LISTENERS

models.addEventListener("click",function(){
	pages.openWindow(imageWin,{animated:true});
});

info.addEventListener("click",function(){
	pages.openWindow(dataWin,{animated:true});
});

video.addEventListener("click",function(){
	pages.openWindow(vidWin,{animated:true});
});



require("secondary");
require("table");
require("custom");
pages.add(models, info, video);  //window.open({modal:true});
mainWin.add(models, info, video);
pages.open(({modal:true}));


